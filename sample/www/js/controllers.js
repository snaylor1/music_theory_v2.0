/**
 * Created by Francis on 16/05/2016.
 */

angular.module('sample.controllers', [])

    .controller('homeCtrl', function($scope, $state){
        $scope.grades = function(){
            $state.go('grades');
        };
        $scope.login = function(){
            $state.go('login');
        };
    })

    .controller('loginCtrl', function($scope, $state){
        $scope.login = function(user){
            console.log('Log-In', user);
            $state.go('grades');
        };
    })



    /*NEEDS TO SYNC TO LOCAL STORAGE TO GET QUESTION DATA*/
    .controller('gradeCtrl', function($scope, $state, $ionicPopup){

        $scope.grades = getGrades();
        for (var i = 0; i < $scope.grades.length ; i++) {
            $scope.grades[i] = {
                gradeid : $scope.grades[i].gradeID,
                name: $scope.grades[i].name,
                unlocked: $scope.grades[i].unlocked
            };
        }

        $scope.goToCourses = function (id) {
          if ($scope.grades[id].unlocked === 1)
            $state.go('courses', {gradeid: id});
          else
            $ionicPopup.alert({
              content: "Complete all courses in previous grade to unlock!"
            })
        };

        $scope.exitApp = function(){
          if (localStorage.getItem("mTheory") !== null)
          {
            localStorage.removeItem("mTheory");
          }
          window.localStorage.setItem("mTheory", saveDB(db.export()));
          db.close();
          if (navigator.app) {
            navigator.app.exitApp();
          }
          else if (navigator.device) {
            navigator.device.exitApp();
          }
          else {
            window.close();
          }
        };
    })



    .controller('courseCtrl', function($scope, $state, $stateParams, $ionicPopup){

      $scope.courses = getCourses($stateParams.gradeid);
      for (var i = 0; i < $scope.courses.length ; i++) {
        $scope.lessons = getLessons($scope.courses[i].courseID);

        $scope.courses[i] = {
          courseID : $scope.courses[i].courseID,
          gradeID: $scope.courses[i].grade_id,
          name: $scope.courses[i].name,
          desc: $scope.courses[i].description,
          lessons: $scope.lessons,
            unlocked: $scope.courses[i].unlocked
        };
      }

      $scope.goToLesson = function(lesson) {
          if (lesson.unlocked == 1) {
              $state.go('lessons', {lessonid: lesson.lessonID});
          }
          else
              $ionicPopup.alert({
              content: "Complete the previous lesson to unlock!"
          })
      };

      $scope.goToTest = function(id) {
        $state.go('tests', { courseid: id });
      };

      $scope.toggleGroup = function(course) {
          if (course.unlocked == 1) {
              if ($scope.isGroupShown(course)) {
                  $scope.shownGroup = null;
              } else {
                  $scope.shownGroup = course;
              }
          } else {
              $ionicPopup.alert({
                  content: "Complete the previous course to unlock!"
              })
          }
      };

      $scope.isGroupShown = function(course) {
        return $scope.shownGroup === course;
      };
    })



    .controller('lessonCtrl', function($scope, $state, $stateParams){
      // load from state, if state doesn't exist initialize
      // always load from question 0, when returning the questions left remove from array
      // keep track of results during lesson, at final question send to results screen
      console.log("we have entered lesson");
      console.log($stateParams);

      $scope.questions = $stateParams.questions;
      $scope.results = $stateParams.results;
      $scope.isTest = false;
      console.log($scope.questions);

      // check if we still have questions
      if ($scope.questions != "NULL") {

        // initialization
        if ($scope.questions[0] == null) {
          // questions is empty
          // initialize
          console.log('init');

          $scope.results = [];

          // get questions for lesson
          $scope.questions = getQuestions($stateParams.lessonid);
          for (var i = 0; i < $scope.questions.length; i++) {
            $scope.questions[i] = {
              id: $scope.questions[i].qID,
              gameType: $scope.questions[i].gameType,
              question: $scope.questions[i].question,
              itemid: $scope.questions[i].item_id,
              correctResponse: $scope.questions[i].correctResponse,
              lessonID: $scope.questions[i].lesson_id,
              items: []
            };
          }
          console.log($scope.questions)
        }

        // prepare question and items for game
        $scope.question = $scope.questions[0];
        $scope.items = getAnswers($scope.question.itemid);
        $scope.question.items = $scope.items;
        console.log("question");
        console.log($scope.question);

        // now that we have extracted the question, we can remove it from questions left to complete
        $scope.questions.splice(0, 1); // at index 0 remove 1 element
        console.log("removed question 0");
        console.log($scope.questions);

        // check if questions is now empty
        if ($scope.questions[0] == null) {
          $scope.questions = "NULL";
        }

        console.log($scope.results);

        // !!! IMPORTANT !!! init the results array, and send it to game
        // check what game to load
        if ($scope.question.gameType == 'mi') {
          console.log("we are leaving lessons");
          $state.go('game-multiimages', {question: $scope.question, lessonid: $stateParams.lessonid, questions: $scope.questions, results: $scope.results, isTest: $scope.isTest });
        } else if ($scope.question.gameType == 'ma') {
          $state.go('game-multichoice', {question: $scope.question, lessonid: $stateParams.lessonid, questions: $scope.questions, results: $scope.results, isTest: $scope.isTest });
        } else if ($scope.question.gameType == 'in') {
          $state.go('game-userinput', {question: $scope.question, lessonid: $stateParams.lessonid, questions: $scope.questions, results: $scope.results, isTest: $scope.isTest });
        } else if ($scope.question.gameType == 're') {
          $state.go('game-recognition', {question: $scope.question, lessonid: $stateParams.lessonid, questions: $scope.questions, results: $scope.results, isTest: $scope.isTest });
        }
      } else {
        console.log("we should go to results now");
        // prepare results and display screen
        // NOTE: make sure everyone is back to default before we leave this page !!!!!!IMPORTANT!!!!!!
        // send results here
        $state.go('results', { results: $stateParams.results, lessonid: $stateParams.lessonid, isTest: $scope.isTest });
        $scope.questions = [];
        $scope.results = [];
      }
    })



    .controller('resultsCtrl', function($scope, $state, $stateParams) {
      $scope.results = $stateParams.results;
      $scope.claimed = getExperience($stateParams.lessonid);
      $scope.questions = getQuestions($stateParams.lessonid);
      $scope.isTest = $stateParams.isTest;

      console.log($scope.results);
      console.log($scope.claimed);
      console.log($scope.questions);

      // calculate and store exp amount
      $scope.expDivided = (100 / $scope.results.length);
      $scope.expAmount = $scope.expDivided;
      $scope.experience = 0;

      $scope.correct = 0;
      $scope.total = $scope.results.length;

      console.log("experience" + $scope.experience);

      for (var i = 0; i < $scope.results.length; i++) {
          if ($scope.results[i] == 1) {
              // the question was correct
              $scope.experience += $scope.expAmount;
              $scope.correct += 1;
              console.log("experience" + $scope.experience);
          } else {
              // the question was incorrect
              if ($scope.claimed[i] == 1) {
                  // the exp has been claimed
                  $scope.experience += $scope.expAmount;
                  console.log("experience" + $scope.experience);
              }
          }
      }

      $scope.goToGrades = function() {
        $state.go('grades');
      }
    })



    .controller('testsCtrl', function($scope, $state, $stateParams) {
      // load from state, if state doesn't exist initialize
      // always load from question 0, when returning the questions left remove from array
      // keep track of results during lesson, at final question send to results screen
      console.log("we have entered test");
      console.log($stateParams);

      $scope.questions = $stateParams.questions;
      $scope.results = $stateParams.results;
      $scope.isTest = true;
      console.log($scope.questions);

      // check if we still have questions
      if ($scope.questions != "NULL") {

        // initialization
        if ($scope.questions[0] == null) {
          // questions is empty
          // initialize
          console.log('init');

          $scope.results = [];

          // get questions for lesson
          $scope.questions = getTestQuestions($stateParams.courseid);
          for (var i = 0; i < $scope.questions.length; i++) {
            $scope.questions[i] = {
              id: $scope.questions[i].qID,
              gameType: $scope.questions[i].gameType,
              question: $scope.questions[i].question,
              itemid: $scope.questions[i].item_id,
              correctResponse: $scope.questions[i].correctResponse,
              lessonID: $scope.questions[i].lesson_id,
              items: []
            };
          }
          console.log($scope.questions)
        }

        // prepare question and items for game
        $scope.question = $scope.questions[0];
        $scope.items = getAnswers($scope.question.itemid);
        $scope.question.items = $scope.items;
        console.log("question");
        console.log($scope.question);

        // now that we have extracted the question, we can remove it from questions left to complete
        $scope.questions.splice(0, 1); // at index 0 remove 1 element
        console.log("removed question 0");
        console.log($scope.questions);

        // check if questions is now empty
        if ($scope.questions[0] == null) {
          $scope.questions = "NULL";
        }

        // !!! IMPORTANT !!! init the results array, and send it to game
        // check what game to load
        if ($scope.question.gameType == 'mi') {
          console.log("we are leaving test");
          $state.go('game-multiimages', {question: $scope.question, lessonid: $stateParams.lessonid, questions: $scope.questions, results: $scope.results, isTest: $scope.isTest });
        } else if ($scope.question.gameType == 'ma') {
          $state.go('game-multichoice', {question: $scope.question, lessonid: $stateParams.lessonid, questions: $scope.questions, results: $scope.results, isTest: $scope.isTest });
        } else if ($scope.question.gameType == 'in') {
          $state.go('game-userinput', {question: $scope.question, lessonid: $stateParams.lessonid, questions: $scope.questions, results: $scope.results, isTest: $scope.isTest});
        } else if ($scope.question.gameType == 're') {
          $state.go('game-recognition', {question: $scope.question, lessonid: $stateParams.lessonid, questions: $scope.questions, results: $scope.results, isTest: $scope.isTest });
        }
      } else {
        console.log("we should go to results now");
        // prepare results and display screen
        // NOTE: make sure everyone is back to default before we leave this page !!!!!!IMPORTANT!!!!!!
        $state.go('results', { results: $stateParams.results, courseid: $stateParams.courseid, isTest: $scope.isTest });
        $stateParams.questions = [];
        $stateParams.results = [];
      }
    })



    .controller('game-multiimagesCtrl', function($scope, $state, $stateParams, $ionicPopup) {
      $scope.shuffle = function(array){
        var currentIndex = array.length, temporaryValue, randomIndex;

        // While there remain elements to shuffle...
        while (0 !== currentIndex) {

          // Pick a remaining element...
          randomIndex = Math.floor(Math.random() * currentIndex);
          currentIndex -= 1;

          // And swap it with the current element.
          temporaryValue = array[currentIndex];
          array[currentIndex] = array[randomIndex];
          array[randomIndex] = temporaryValue;
        }

        return array;
      };

      $scope.question = $stateParams.question;
      $scope.results = $stateParams.results;

      $scope.question.items = $scope.shuffle($scope.question.items);
      $scope.question.items = $scope.shuffle($scope.question.items);
      console.log("current:" , $scope.question.items);

      console.log("inside minigame ctrl");
      console.log("LOOK HERE", $stateParams);

      // My own simple code for checking answer
      $scope.answerQuestion = function(id) {
        if (id == $scope.question.itemid) {
          // the answer was correct
          // display response on screen
          // add correct to the results array and db
          $scope.results.push(1);

          if ($stateParams.isTest == false)
            setExperience(1, $scope.question.id, 1);

          // display response on screen
          /*$ionicPopup.alert({
            content: "Correct!"
          })*/
        } else {
          // the answer was incorrect
          // display response on screen
          // add incorrect to the results array and db
          $scope.results.push(0);

          if ($stateParams.isTest == false)
            setExperience(1, $scope.question.id, 0);

          // display response on screen
          /*$ionicPopup.alert({
            content: "Wrong!"
          })*/
        }

        // NOTE: this should be moved into another function, it should be called after the response has been put on the screen
        //       the user will tap the screen to advance (overlay on screen) OR press continue button on response
        console.log("leaving mini game");
          if ($stateParams.isTest == 0)
            $state.go('lessons', { lessonid: $stateParams.lessonid, questions: $stateParams.questions, results: $scope.results, isTest: $stateParams.isTest });
          else
            $state.go('tests', { lessonid: $stateParams.lessonid, questions: $stateParams.questions, results: $scope.results, isTest: $stateParams.isTest });
      };

      $scope.close = function(){
        $state.go('grades');
      }
    })

    .controller('game-multichoiceCtrl', function($scope, $state, $stateParams, $ionicPopup) {
      $scope.shuffle = function(array){
        var currentIndex = array.length, temporaryValue, randomIndex;

        // While there remain elements to shuffle...
        while (0 !== currentIndex) {

          // Pick a remaining element...
          randomIndex = Math.floor(Math.random() * currentIndex);
          currentIndex -= 1;

          // And swap it with the current element.
          temporaryValue = array[currentIndex];
          array[currentIndex] = array[randomIndex];
          array[randomIndex] = temporaryValue;
        }

        return array;
      };

      $scope.question = $stateParams.question;
      $scope.results = $stateParams.results;

      console.log("inside minigame ctrl");
      console.log($scope.question);

      $scope.question.items = $scope.shuffle($scope.question.items);
      $scope.question.items = $scope.shuffle($scope.question.items);
      console.log("current:" , $scope.question.items);

      // My own simple code for checking answer
      $scope.answerQuestion = function(id) {
        if (id == $scope.question.itemid) {
          // the answer was correct
          // display response on screen
          // add correct to the results array and db
          $scope.results.push(1);

          if ($stateParams.isTest == false)
            setExperience(1, $scope.question.id, 1);

          // display response on screen
          /*$ionicPopup.alert({
            content: "Correct!"
          })*/
        } else {
          // the answer was incorrect
          // display response on screen
          // add incorrect to the results array and db
          $scope.results.push(0);

          if ($stateParams.isTest == false)
            setExperience(1, $scope.question.id, 0);

          // display response on screen
          /*$ionicPopup.alert({
            content: "Wrong!"
          })*/
        }

        // NOTE: this should be moved into another function, it should be called after the response has been put on the screen
        //       the user will tap the screen to advance (overlay on screen) OR press continue button on response
        console.log("leaving mini game");
          if ($stateParams.isTest == 0)
              $state.go('lessons', { lessonid: $stateParams.lessonid, questions: $stateParams.questions, results: $scope.results, isTest: $scope.isTest });
          else
              $state.go('tests', { lessonid: $stateParams.lessonid, questions: $stateParams.questions, results: $scope.results, isTest: $scope.isTest });
      };

      $scope.close = function(){
        $state.go('grades');
      }
    })

    .controller('game-userinputCtrl', function($scope, $state, $stateParams, $ionicPopup) {
      $scope.question = $stateParams.question;
      $scope.results = $stateParams.results;

      $scope.input = { value: null };

      $scope.answerQuestion = function(input) {
        if (input.toLowerCase().trim() == $scope.question.items[0].name.toLowerCase()) {
          // the answer was correct
          // display response on screen
          // add correct to the results array and db
          $scope.results.push(1);

          if ($stateParams.isTest == false)
            setExperience(1, $scope.question.id, 1);

          // display response on screen
          /*$ionicPopup.alert({
            content: "Correct!"
          })*/
        } else {
          // the answer was incorrect
          // display response on screen
          // add incorrect to the results array and db
          $scope.results.push(0);

          if ($stateParams.isTest == false)
            setExperience(1, $scope.question.id, 0);

          // display response on screen
          /*$ionicPopup.alert({
            content: "Wrong!"
          })*/
        }

        // NOTE: this should be moved into another function, it should be called after the response has been put on the screen
        //       the user will tap the screen to advance (overlay on screen) OR press continue button on response
        console.log("leaving mini game");
          if ($stateParams.isTest == 0)
              $state.go('lessons', { lessonid: $stateParams.lessonid, questions: $stateParams.questions, results: $scope.results, isTest: $scope.isTest });
          else
              $state.go('tests', { lessonid: $stateParams.lessonid, questions: $stateParams.questions, results: $scope.results, isTest: $scope.isTest });
      };

      $scope.close = function(){
        $state.go('grades');
      }
    })

    .controller('game-recognitionCtrl', function($scope, $state, $stateParams, $ionicPopup) {
      $scope.shuffle = function(array){
        var currentIndex = array.length, temporaryValue, randomIndex;

        // While there remain elements to shuffle...
        while (0 !== currentIndex) {

          // Pick a remaining element...
          randomIndex = Math.floor(Math.random() * currentIndex);
          currentIndex -= 1;

          // And swap it with the current element.
          temporaryValue = array[currentIndex];
          array[currentIndex] = array[randomIndex];
          array[randomIndex] = temporaryValue;
        }

        return array;
      };

      $scope.question = $stateParams.question;
      $scope.results = $stateParams.results;

      console.log("inside minigame ctrl");
      console.log($scope.question);

      $scope.question.items = $scope.shuffle($scope.question.items);
      $scope.question.items = $scope.shuffle($scope.question.items);
      console.log("current:" , $scope.question.items);

      // My own simple code for checking answer
      $scope.answerQuestion = function(id) {
        if (id == $scope.question.itemid) {
          // the answer was correct
          // display response on screen
          // add correct to the results array and db
          $scope.results.push(1);
          if ($stateParams.isTest == false)
            setExperience(1, $scope.question.id, 1);

          // display response on screen
          /*$ionicPopup.alert({
            content: "Correct!"
          })*/
        } else {
          // the answer was incorrect
          // display response on screen
          // add incorrect to the results array and db
          $scope.results.push(0);

          if ($stateParams.isTest == false)
            setExperience(1, $scope.question.id, 0);

          // display response on screen
          /*$ionicPopup.alert({
            content: "Wrong!"
          })*/
        }

        // NOTE: this should be moved into another function, it should be called after the response has been put on the screen
        //       the user will tap the screen to advance (overlay on screen) OR press continue button on response
        console.log("leaving mini game");
          if ($stateParams.isTest == 0)
              $state.go('lessons', { lessonid: $stateParams.lessonid, questions: $stateParams.questions, results: $scope.results, isTest: $scope.isTest });
          else
              $state.go('tests', { lessonid: $stateParams.lessonid, questions: $stateParams.questions, results: $scope.results, isTest: $scope.isTest });
      };

      $scope.close = function(){
        $state.go('grades');
      }
    });
