/**
 * Created by Francis on 16/05/2016.
 */
angular.module('sample.services', [])

.factory('DatabaseService', function() {

    var _createDB = function(db){
        //turn foreign key constraints on
        db.run("PRAGMA foreign_keys = ON");

        //grade table
        db.run("create table if not exists grade(gradeID integer primary key autoincrement,"+
            "name varchar(15) not null default 0)");

        //course table
        db.run("create table if not exists course(courseID integer primary key autoincrement,"+
            "grade_id integer not null,"+
            "name varchar(15) not null default 0,"+
            "description text not null,"+
            "foreign key (grade_id) references grade(gradeID))");

        //lesson table
        db.run("create table if not exists lesson(lessonID integer primary key autoincrement,"+
            "course_id integer not null,"+
            "name varchar(15) not null default 0,"+
            "description text not null,"+
            "foreign key (course_id) references course(courseID))");

        //item table
        db.run("create table if not exists item(itemID integer primary key autoincrement,"+
            "name varchar(15) not null,"+
            "image varchar(20) not null unique,"+
            "toolTip varchar(30) not null,"+
            "description text not null,"+
            "similarity unsigned tinyint not null default 0)");

        //question table
        db.run("create table if not exists question(qID integer primary key autoincrement,"+
            "lesson_id integer not null,"+
            "item_id integer not null,"+
            "question text not null default 0,"+
            "correctResponse text not null default 'Correct!',"+
            "gameType char(2) check (gameType in ('mi', 'ma')),"+
            "foreign key (lesson_id) references lesson(lessonID),"+
            "foreign key (item_id) references item(itemID))");

        //user table
        db.run("create table if not exists user(userID integer primary key autoincrement,"+
            "name varchar(30) not null,"+
            "email varchar(30) not null,"+
            "xp unsigned smallint not null default 0,"+
            "picture varchar(20) not null default 'default.jpg')");

        //question_user table
        db.run("create table if not exists question_user(user_id integer not null,"+
            "question_id integer not null,"+
            "completed boolean not null,"+
            "foreign key (user_id) references user(userID),"+
            "foreign key (question_id) references question(qID))");

        //grade data
        db.run("INSERT INTO grade VALUES (?,?), (?, ?)",
            [0, "Preliminary",
                1, "Grade 1"]);

        //course data
        db.run("INSERT INTO course VALUES (?, ?, ?, ?), (?, ?, ?, ?)",
            [1, 1, "Course1", "C1Desc",
                2, 1, "Course2", "C2Desc"]);

        //lesson data
        db.run("INSERT INTO lesson VALUES (?, ?, ?, ?), (?, ?, ?, ?)",
            [1, 1, "Lesson1", "L1desc",
                2, 1, "Lesson2", "L2desc"]);

        //item data
        db.run("INSERT INTO item VALUES (?, ?, ?, ?, ?, ?), (?, ?, ?, ?, ?, ?), (?, ?, ?, ?, ?, ?), (?, ?, ?, ?, ?, ?)",
            [1, "Treble clef", "Clefs/Treble clef.png", "tooltip1", "descript1", 0,
                2, "Bass clef", "Clefs/Bass Clef.png", "tooltip2", "descript2", 0,
                3, "Stave", "Staves/Stave lines1.png", "tooltip3", "descript3", 0,
                4, "Semibreve", "Notes/Semibreve/Semibreve.png", "tooltip4", "descript4", 0]);

        //question data
        db.run("INSERT INTO question VALUES (?, ?, ?, ?, ?, ?), (?, ?, ?, ?, ?, ?), (?, ?, ?, ?, ?, ?), (?, ?, ?, ?, ?, ?)",
            [1, 1, 1, "Which image is the treble clef?", "Correct. That is the treble clef.", "mi",
                2, 1, 2, "Which image is the bass clef?", "Correct. That is the bass clef.", "mi",
                3, 1, 3, "Which image is the stave?", "Correct. That is the stave.", "mi",
                4, 1, 4, "Which image is the semibreve", "Correct. That is the semibreve", "mi"]);

        //user data - TEMPORARY - FOR TEST USES ONLY
        db.run("INSERT INTO user VALUES (?, ?, ?, ?, ?)",
            [1, "user1", "email1", 0, "profile.jpg"]);
    };

    var _loadDB = function(str){
        var l = str.length,
            arr = new Uint8Array(l);
        for (var i = 0; i < l; i++) arr[i] = str.charCodeAt(i);
        return arr;
    };

    //save database to memory
    var _saveDB = function(arr){
        var uarr = new Uint8Array(arr);
        var strings = [], chunksize = 0xffff;
        // There is a maximum stack size. We cannot call String.fromCharCode with as many arguments as we want
        for (var i = 0; i * chunksize < uarr.length; i++) {
            strings.push(String.fromCharCode.apply(null, uarr.subarray(i * chunksize, (i + 1) * chunksize)));
        }
        return strings.join('');
    };

    var _getGrades = function(){
        var stmt = db.prepare("select * from grade");
        var grades = [];
        while (stmt.step())
        {
            var row = stmt.getAsObject();
            grades.push(row);
        }
        stmt.free();
        //returns an array of objects grades[x].colName = value
        return grades;

        /* to get values from these, should look like this
         for(var i = 0; i < grades.length; i++)
         {
         document.writeln(grades[i].gradeID);
         document.writeln(grades[i].name);
         }
         */
    };

    //get all courses from a specific grade
    var _getCourses = function(grade){
        var stmt = db.prepare("select * from course where grade_id = $grade");
        stmt.bind({ $grade: grade });
        var courses = [];
        while (stmt.step())
        {
            var row = stmt.getAsObject();
            courses.push(row);
        }
        stmt.free();
        //returns an array of objects courses[x].colName = value
        return courses;

        /* to get values from these, should look like this
         for (var i = 0; i < courses.length; i++)
         {
         document.writeln(courses[i].courseID);
         document.writeln(courses[i].name);
         document.writeln(courses[i].description);
         }
         */
    };

    //get all lessons from a specific course
    var _getLessons = function(course){
        var stmt = db.prepare("select * from lesson where course_id = $course");
        stmt.bind({ $course: course });
        var lessons = [];
        while (stmt.step())
        {
            var row = stmt.getAsObject();
            lessons.push(row);
        }
        stmt.free();
        //returns an array of objects lessons[x].colName = value
        return lessons;

        /* to get values from these, should look like this
         for (var i = 0; i < lessons.length; i++)
         {
         document.writeln(lessons[i].lessonID);
         document.writeln(lessons[i].name);
         document.writeln(lessons[i].description);
         }
         */
    };

    //get a specific question
    var _getQuestion = function(lesson, question){
        var stmt = db.prepare("select question.* from question, lesson, course where lesson_id = :lesson and qID = :question;");
        var questionRow = stmt.getAsObject({ ':lesson': lesson, ':question': question });

        stmt.free();
        return questionRow;
    };

    //get a specific item
    var _getItem = function(item){
        stmt = db.prepare("select * from item where itemID = :item");
        var itemRow = stmt.getAsObject({ ':item': item });

        stmt.free();
        return itemRow;
    };

    return {
        createDB : _createDB,
        loadDB: _loadDB,
        saveDB: _saveDB,
        getGrades : _getGrades,
        getCourses : _getCourses,
        getLessons: _getLessons,
        getQuestion : _getQuestion,
        getItem : _getItem        
    }

});