// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'

angular.module('sample', ['ionic', 'ui.router', 'sample.controllers'])

    .run(function($ionicPlatform) {
        $ionicPlatform.ready(function() {
            if(window.cordova && window.cordova.plugins.Keyboard) {
              // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
              // for form inputs)
              cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);

              // Don't remove this line unless you know what you are doing. It stops the viewport
              // from snapping when text inputs are focused. Ionic handles this internally for
              // a much nicer keyboard experience.
              cordova.plugins.Keyboard.disableScroll(true);
            }
            if(window.StatusBar) {
              StatusBar.styleDefault();
            }

            window.onbeforeunload = function ()
            {
            	if (localStorage.getItem("mTheory") !== null)
            	{
            		localStorage.removeItem("mTheory");
            	}
            	window.localStorage.setItem("mTheory", saveDB(db.export()));
            	db.close();
            	ionic.Platform.exitApp();
            };

            navigator.splashscreen.hide();
      })
    })

    .config(function($stateProvider, $urlRouterProvider){

        $stateProvider
            .state('index', {
                url: '/',
                abstract: false,
                templateUrl: 'partials/home.html',
                controller: 'homeCtrl'
            })
            .state('login', {
                url: '/login',
                templateUrl: 'partials/login.html',
                controller: 'loginCtrl'
            })
            .state('grades', {
                url: '/grades',
                templateUrl: 'partials/grades.html',
                controller: 'gradeCtrl'
            })
            .state('courses', {
                url: '/courses',
                templateUrl: 'partials/courses.html',
                controller: 'courseCtrl',
                params: { gradeid: null }
            })
            .state('lessons', {
                url: '/lessons',
                templateUrl: 'partials/lessons.html',
                controller: 'lessonCtrl',
                cache: false,
                params: { questions: [],
                          lessonid: null,
                          results: null,
                          isTest: null }
            })

            .state('tests', {
              url: '/tests',
              templateUrl: 'partials/tests.html',
              controller: 'testsCtrl',
                cache: false,
              params: { questions: [],
                        courseid: null,
                        results: null,
                        isTest: null}
            })

            .state('results', {
                url: '/results',
                templateUrl: 'partials/results.html',
                controller: 'resultsCtrl',
                cache: false,
                params: { results: null,
                          lessonid: null,
                          isTest: null }
            })

            .state('game-multichoice', {
              url: '/game-multichoice',
              templateUrl: 'partials/game-multichoice.html',
              controller: 'game-multichoiceCtrl',
              cache: false,
              params: { question: null,
                        questions: null,
                        lessonid: null,
                        results: null,
                        isTest: null }
            })

            .state('game-multiimages', {
              url: '/game-multiimages',
              templateUrl: 'partials/game-multiimages.html',
              controller: 'game-multiimagesCtrl',
              cache: false,
              params: { question: null,
                        questions: null,
                        lessonid: null,
                        results: null,
                        isTest: null }
            })

            .state('game-userinput', {
              url: '/game-userinput',
              templateUrl: 'partials/game-userinput.html',
              controller: 'game-userinputCtrl',
              cache: false,
              params: { question: null,
                        questions: null,
                        lessonid: null,
                        results: null,
                        isTest: null  }
            })

            .state('game-recognition', {
              url: '/game-recognition',
              templateUrl: 'partials/game-recognition.html',
              controller: 'game-recognitionCtrl',
              cache: false,
              params: { question: null,
                questions: null,
                lessonid: null,
                results: null,
                isTest: null  }
            });

        $urlRouterProvider.otherwise('/');

    });
