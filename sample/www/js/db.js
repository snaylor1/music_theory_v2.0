//This file uses the sql.js plugin from https://github.com/kripken/sql.js/
//Some testing functions are located at the end of the file
//To add extra items and not mess with the item_id in the questions table - append them to the end of the item inserts

//This removes local storage item for when you update this file - simply uncomment this and run it 
//localStorage.removeItem("mTheory");

//create and populate a new database
function createDB(db)
{
    //turn foreign key constraints on
    db.run("PRAGMA foreign_keys = ON");

    //grade table
    db.run("create table if not exists grade(gradeID integer primary key autoincrement,"+
        "name varchar(15) not null default 0,"+
        "unlocked boolean default 0)");

    //course table
    db.run("create table if not exists course(courseID integer primary key autoincrement,"+
	    "grade_id integer not null,"+
	    "name varchar(15) not null default 0,"+
	    "description text not null," +
        "unlocked boolean default 0," +
	    "foreign key (grade_id) references grade(gradeID))");

    //lesson table
    db.run("create table if not exists lesson(lessonID integer primary key autoincrement,"+
	    "course_id integer not null,"+
	    "name varchar(15) not null default 0,"+
	    "description text not null," +
        "unlocked boolean default 0," +
	    "foreign key (course_id) references course(courseID))");

    //item table
    db.run("create table if not exists item(itemID integer primary key autoincrement,"+
	    "name varchar(15) not null,"+
	    "image varchar(20) not null,"+
	    "similarity unsigned tinyint not null default 0)");

    //question table
    db.run("create table if not exists question(qID integer primary key autoincrement,"+
	    "lesson_id integer not null,"+
	    "item_id integer not null,"+
	    "question text not null default 0,"+
	    "gameType char(2) check (gameType in ('mi', 'ma', 'in', 're')),"+
	    "foreign key (lesson_id) references lesson(lessonID),"+
	    "foreign key (item_id) references item(itemID))");

    //user table
    db.run("create table if not exists user(userID integer primary key autoincrement,"+
	    "name varchar(30) not null,"+
	    "email varchar(30) not null,"+
	    "xp integer not null default 0,"+
	    "picture varchar(20) not null default 'default.jpg')");

    //question_user table
    db.run("create table if not exists question_user(user_id integer not null,"+
	    "question_id integer not null,"+
	    "completed boolean not null,"+
	    "foreign key (user_id) references user(userID),"+
	    "foreign key (question_id) references question(qID))");

    //grade data
    db.run("INSERT INTO grade VALUES (?, ?, ?), (?, ?, ?)",
        [0, "Preliminary", 1,
        null, "Grade 1", 0]);

    //course data
    db.run("INSERT INTO course VALUES (?, ?, ?, ?, ?), (?, ?, ?, ?, ?)",
        [null, 0, "Course1", "The basics", 1,
        null, 0, "Course2", "Modifiers", 0]);

    //lesson data - course 1
    db.run("INSERT INTO lesson VALUES (?, ?, ?, ?, ?), (?, ?, ?, ?, ?), (?, ?, ?, ?, ?), (?, ?, ?, ?, ?), (?, ?, ?, ?, ?)",
        [null, 1, "Introduction", "Basic symbols", 1,
        null, 1, "Durations", "Beats taken for notes", 0,
        null, 1, "Treble notes", "Every Good Boy Deserves Fruit. FACE", 0,
        null, 1, "Bass notes", "Good Boys Don't Fight Anymore. ACEG", 0,
        null, 1, "Review", "Memorization", 0]);

    //lesson data - course 2
    db.run("INSERT INTO lesson VALUES (?, ?, ?, ?, ?), (?, ?, ?, ?, ?), (?, ?, ?, ?, ?)",
        [null, 2, "Bar lines", "Distinguishing measures", 0,
        null, 2, "Accidentals", "Sharp, flat, natural introduction", 0,
        null, 2, "Recognition", "Recognizing accidentals", 0]);

    //item data - similarity 1 - Intro
    db.run("INSERT INTO item VALUES (?, ?, ?, ?), (?, ?, ?, ?), (?, ?, ?, ?), (?, ?, ?, ?), (?, ?, ?, ?)",
        [null, "Treble clef", "Clefs/Treble clef.png", 1,
        null, "Bass clef", "Clefs/Bass Clef.png", 1,
        null, "Drums", "Dummy/drums.png", 1,
        null, "Trumpet", "Dummy/trumpet.png", 1,
        null, "Violin", "Dummy/violin.png", 1]);

    //item data - similarity 2 - Notes
    db.run("INSERT INTO item VALUES (?, ?, ?, ?), (?, ?, ?, ?), (?, ?, ?, ?), (?, ?, ?, ?), (?, ?, ?, ?), (?, ?, ?, ?), (?, ?, ?, ?), (?, ?, ?, ?)",
        [null, "Semibreve", "Notes/Semibreve/Semibreve.png", 2,
        null, "Minim", "Notes/Minims/Minim.png", 2,
        null, "Crotchet", "Notes/Crotchets/Crotchet.png", 2,
        null, "Quaver", "Notes/Quavers/Quaver single.png", 2,
        null, "Two quavers joined", "Notes/Quavers/Quavers 2 joined.png", 2,
        null, "Four quavers joined", "Notes/Quavers/Quavers 4 joined.png", 2,
        null, "Demisemiquaver", "Notes/Quavers/demisemiquaver.png", 2,
        null, "Semiquaver", "Notes/Quavers/Semiquaver.png", 2]);

    //item data - similarity 3
    db.run("INSERT INTO item VALUES (?, ?, ?, ?), (?, ?, ?, ?), (?, ?, ?, ?), (?, ?, ?, ?), (?, ?, ?, ?), (?, ?, ?, ?), (?, ?, ?, ?), (?, ?, ?, ?), (?, ?, ?, ?)",
        [null, "Stave", "Staves/Stave lines1.png", 3,
        null, "Leger lines", "Staves/Leger Lines.png", 3,
        null, "Stem", "Notes/Crotchets/Headless/Crotchet.png", 3,
        null, "Beam", "Notes/Semiquavers/Headless/Semiquavers 2 joined.png", 3,
        null, "Double bar line", "Barlines/Double Barline.png", 3,
        null, "Bar line", "Barlines/Barline.png", 3,
        null, "Barline final", "Barlines/Barline final.png", 3,
        null, "Bar line start repeat", "Barlines/Start repeat.png", 3,
        null, "Bar line end repeat", "Barlines/End repeat.png", 3]);

    //item data - similarity 4 - durations
    db.run("INSERT INTO item VALUES (?, ?, ?, ?), (?, ?, ?, ?), (?, ?, ?, ?), (?, ?, ?, ?), (?, ?, ?, ?), (?, ?, ?, ?)",
        [null, "4 beats", "Notes/Semibreve/Semibreve.png", 4,
        null, "2 beats", "Notes/Minims/Minim.png", 4,
        null, "1 beat", "Notes/Crotchets/Crotchet.png", 4,
        null, "Half beat", "Notes/Quavers/Quaver single.png", 4,
        null, "Quarter beat", "Notes/Quavers/Semiquaver.png", 4,
        null, "Eighth beat", "Notes/Quavers/demisemiquaver.svg", 4]);

    //item data - similarity 5 - EGBDF FACE
    db.run("INSERT INTO item VALUES (?, ?, ?, ?), (?, ?, ?, ?), (?, ?, ?, ?), (?, ?, ?, ?), (?, ?, ?, ?), (?, ?, ?, ?), (?, ?, ?, ?), (?, ?, ?, ?), (?, ?, ?, ?)",
        [null, "E", "Staves/treble_e.png", 5,
        null, "G", "Staves/treble_g.png", 5,
        null, "B", "Staves/treble_b.png", 5,
        null, "D", "Staves/treble_d.png", 5,
        null, "F", "Staves/treble_f2.png", 5,
        null, "F", "Staves/treble_f.png", 5,
        null, "A", "Staves/treble_a.png", 5,
        null, "C", "Staves/treble_c.png", 5,
        null, "E", "Staves/treble_e2.png", 5]);

    //item data - similarity 6 - GBDFA ACEG
    db.run("INSERT INTO item VALUES (?, ?, ?, ?), (?, ?, ?, ?), (?, ?, ?, ?), (?, ?, ?, ?), (?, ?, ?, ?), (?, ?, ?, ?), (?, ?, ?, ?), (?, ?, ?, ?), (?, ?, ?, ?), (?, ?, ?, ?)",
        [null, "G", "Staves/bass_g.png", 6,
        null, "B", "Staves/bass_b.png", 6,
        null, "D", "Staves/bass_d.png", 6,
        null, "F", "Staves/bass_f.png", 6,
        null, "A", "Staves/bass_a2.png", 6,
        null, "A", "Staves/bass_a.png", 6,
        null, "C", "Staves/bass_c.png", 6,
        null, "E", "Staves/bass_e.png", 6,
        null, "G", "Staves/bass_g2.png", 6,
        null, "Semitone", "Staves/semitone.png", 6]);

    //item data - similarity 7 - Accidentals
    db.run("INSERT INTO item VALUES (?, ?, ?, ?), (?, ?, ?, ?), (?, ?, ?, ?), (?, ?, ?, ?), (?, ?, ?, ?)",
        [null, "Sharp", "Accidentals/Sharp.png", 7,
        null, "Flat", "Accidentals/Flat.png", 7,
        null, "Natural", "Accidentals/Natural.png", 7,
        null, "Double flat", "Accidentals/Double flat.png", 7,
        null, "Double Sharp", "Accidentals/Double sharp.png", 7]);

    //question data - lesson 1
    db.run("INSERT INTO question VALUES (?, ?, ?, ?, ?), (?, ?, ?, ?, ?), (?, ?, ?, ?, ?), (?, ?, ?, ?, ?), (?, ?, ?, ?, ?), (?, ?, ?, ?, ?), (?, ?, ?, ?, ?), (?, ?, ?, ?, ?), (?, ?, ?, ?, ?), (?, ?, ?, ?, ?)",
        [null, 1, 1, "Which image is the treble clef?", "mi",
        null, 1, 2, "Which image is the bass clef?", "mi",
        null, 1, 14, "Which image is the stave?", "mi",
        null, 1, 6, "Which image is the semibreve", "mi",
        null, 1, 7, "Which image is the minim", "mi",
        null, 1, 8, "Which image is the crotchet", "mi",
        null, 1, 9, "Which image is the quaver", "mi",
        null, 1, 10, "Which image is the two quavers joined", "mi",
        null, 1, 11, "Which image is the four quavers joined", "mi",
        null, 1, 15, "Which image is the leger lines", "mi"]);

    //question data - lesson 2
    db.run("INSERT INTO question VALUES (?, ?, ?, ?, ?), (?, ?, ?, ?, ?), (?, ?, ?, ?, ?), (?, ?, ?, ?, ?), (?, ?, ?, ?, ?), (?, ?, ?, ?, ?), (?, ?, ?, ?, ?), (?, ?, ?, ?, ?), (?, ?, ?, ?, ?), (?, ?, ?, ?, ?)",
        [null, 2, 23, "What is the note duration of the semibreve?", "ma",
        null, 2, 24, "What is the note duration of a minim if it is half a semibreve?", "ma",
        null, 2, 25, "What is the note duration of a crotchet if it is half a minim?", "ma",
        null, 2, 26, "What is the note duration of a quaver if it is half a crotchet?", "ma",
        null, 2, 25, "What is the note duration of two quavers joined?", "ma",
        null, 2, 24, "What is the note duration of four quavers joined?", "ma",
        null, 2, 27, "What is the note duration of a semiquaver if it is half a quaver?", "ma",
        null, 2, 28, "What is the note duration of a demisemiquaver if it is half a semiquaver?", "ma",
        null, 2, 16, "What is the name of the line going up or down a note which defines its' duration?", "ma",
        null, 2, 17, "What is the name of the line that connects multiple quavers at the top to define its' duration?", "ma"]);

    //question data - lesson 3 - treble notes
    db.run("INSERT INTO question VALUES (?, ?, ?, ?, ?), (?, ?, ?, ?, ?), (?, ?, ?, ?, ?), (?, ?, ?, ?, ?), (?, ?, ?, ?, ?), (?, ?, ?, ?, ?), (?, ?, ?, ?, ?), (?, ?, ?, ?, ?), (?, ?, ?, ?, ?)",
        [null, 3, 29, "Which image is the E?", "mi",
        null, 3, 30, "Which image is the G?", "mi",
        null, 3, 31, "Which image is the B", "mi",
        null, 3, 32, "Which image is the D?", "mi",
        null, 3, 33, "Which image is the F?", "mi",
        null, 3, 34, "Which image is the F?", "mi",
        null, 3, 35, "Which image is the A?", "mi",
        null, 3, 36, "Which image is the C?", "mi",
        null, 3, 37, "Which image is the E?", "mi"]);

    //question data - lesson 4 - bass notes
    db.run("INSERT INTO question VALUES (?, ?, ?, ?, ?), (?, ?, ?, ?, ?), (?, ?, ?, ?, ?), (?, ?, ?, ?, ?), (?, ?, ?, ?, ?), (?, ?, ?, ?, ?), (?, ?, ?, ?, ?), (?, ?, ?, ?, ?), (?, ?, ?, ?, ?)",
        [null, 4, 38, "Which image is the G note?", "mi",
        null, 4, 39, "Which image is the B note?", "mi",
        null, 4, 40, "Which image is the D note?", "mi",
        null, 4, 41, "Which image is the F note?", "mi",
        null, 4, 42, "Which image is the A note?", "mi",
        null, 4, 43, "Which image is the A note?", "mi",
        null, 4, 44, "Which image is the C note?", "mi",
        null, 4, 45, "Which image is the E note?", "mi",
        null, 4, 46, "Which image is the G note?", "mi"]);

    //question data - lesson 5
    db.run("INSERT INTO question VALUES (?, ?, ?, ?, ?), (?, ?, ?, ?, ?), (?, ?, ?, ?, ?), (?, ?, ?, ?, ?), (?, ?, ?, ?, ?), (?, ?, ?, ?, ?)",
        [null, 5, 1, "Which clef uses the mnemonic Every Good Boy Doesn't fight?", "in",
        null, 5, 14, "What are the 5 lines called?", "in",
        null, 5, 6, "Which note takes 4 beats?", "in",
        null, 5, 13, "Which note takes a quarter beat?", "in",
        null, 5, 2, "Which clef uses the mnemonic Good Boys Don't Fight Anymore?", "in",
        null, 5, 16, "What is the line connected to a note that defines its duration called?", "in"]);

    //question data - lesson 6
    db.run("INSERT INTO question VALUES (?, ?, ?, ?, ?), (?, ?, ?, ?, ?), (?, ?, ?, ?, ?), (?, ?, ?, ?, ?), (?, ?, ?, ?, ?)",
        [null, 6, 19, "Which image shows the bar line?", "mi",
        null, 6, 18, "Which image shows the double bar line", "mi",
        null, 6, 20, "Which image shows the final bar line", "mi",
        null, 6, 21, "Which image shows the begin repeat bar line", "mi",
        null, 6, 22, "Which image shows the end repeat bar line", "mi"]);

    //question data - lesson 7
    db.run("INSERT INTO question VALUES (?, ?, ?, ?, ?), (?, ?, ?, ?, ?), (?, ?, ?, ?, ?), (?, ?, ?, ?, ?), (?, ?, ?, ?, ?), (?, ?, ?, ?, ?)",
        [null, 7, 47, "Which image shows a semitone?", "mi",
        null, 7, 48, "Which image shows a sharp symbol", "mi",
        null, 7, 49, "Which image shows a flat symbol", "mi",
        null, 7, 52, "Which image shows a natural symbol", "mi",
        null, 7, 51, "Which image shows a double flat symbol", "mi",
        null, 7, 52, "Which image shows a double sharp symbol", "mi"]);

    //question data - lesson 8
    db.run("INSERT INTO question VALUES (?, ?, ?, ?, ?), (?, ?, ?, ?, ?), (?, ?, ?, ?, ?), (?, ?, ?, ?, ?), (?, ?, ?, ?, ?), (?, ?, ?, ?, ?)",
        [null, 8, 47, "Which symbol is shown here?", "re",
        null, 8, 48, "Which symbol is shown here?", "re",
        null, 8, 49, "Which symbol is shown here?", "re",
        null, 8, 52, "Which symbol is shown here?", "re",
        null, 8, 51, "Which symbol is shown here?", "re",
        null, 8, 52, "Which symbol is shown here?", "re"]);

    //user data
    db.run("INSERT INTO user VALUES (?, ?, ?, ?, ?)",
        [null, "user1", "email1", 0, "profile.jpg"]);
}

//get all grades
function getGrades()
{
    var stmt = db.prepare("select * from grade");
        var grades = [];
        while (stmt.step())
        {
            var row = stmt.getAsObject();
        grades.push(row);
    }
    stmt.free();

    return grades;

    /* This is how this sort of data would be accessed
    to get values from these, it should look like this
    for(var i = 0; i < grades.length; i++)
    {
        document.writeln(grades[i].gradeID);
        document.writeln(grades[i].name);
    }
    */
}

//get all courses from a specific grade
function getCourses(grade)
{
    var stmt = db.prepare("select * from course where grade_id = $grade");
    stmt.bind({ $grade: grade });
    var courses = [];
    while (stmt.step())
    {
        var row = stmt.getAsObject();
        courses.push(row);
    }
    stmt.free();

    return courses;
}

//get all lessons from a specific course
function getLessons(course)
{
    var stmt = db.prepare("select * from lesson where course_id = $course");
    stmt.bind({ $course: course });
    var lessons = [];
    while (stmt.step())
    {
        var row = stmt.getAsObject();
        lessons.push(row);
    }
    stmt.free();

    return lessons;
}

//get a specific question
function getQuestion(lesson, question)
{
    var stmt = db.prepare("select question.* from question, lesson, course where lesson_id = :lesson and qID = :question;");
    var questionRow = stmt.getAsObject({ ':lesson': lesson, ':question': question });

    stmt.free();
    return questionRow;
}

//get all the questions of a lesson
function getQuestions(lesson)
{
    var stmt = db.prepare("select question.* from question where lesson_id = $lesson;");
    stmt.bind({ $lesson: lesson });
    var questions = [];
    while (stmt.step())
    {
        var row = stmt.getAsObject();
        questions.push(row);
    }
    stmt.free();

    return questions;
}

//get 20 questions from a specific course for a test
function getTestQuestions(course)
{
    var stmt = db.prepare("select question.* from question, lesson where course_id = $course and lessonID = lesson_id order by random() limit 20");
    stmt.bind({ $course: course });
    var questions = [];
    while (stmt.step()) {
        var row = stmt.getAsObject();
        questions.push(row);
    }
    stmt.free();

    return questions;
}

//get 4 similar items to populate question
function getAnswers(item)
{
    //gets the correct answer to a question
    var stmt = db.prepare("select * from item where itemID = $item");
    stmt.bind({ $item: item });
    var answers = [];
    while (stmt.step()) {
        var row = stmt.getAsObject();
        answers.push(row);
    }
    stmt.free();

    var similarity = answers[0].similarity;

    //gets 3 incorrect answers
    var stmt = db.prepare("select * from item where similarity = $similarity and itemID != $item order by random() limit 3");
    stmt.bind({ $similarity: similarity, $item: item });
    while (stmt.step()) {
        var row = stmt.getAsObject();
        answers.push(row);
    }
    stmt.free();

    //if 4 answers weren't grabbed, get the remainder randomly
    if (answers.length != 4)
    {
        var required = 4 - answers.length;
        var stmt = db.prepare("select * from item where similarity != $similarity and itemID != $item order by random() limit $required");
        stmt.bind({ $similarity: similarity, $item: item, $required: required });
        while (stmt.step()) {
            var row = stmt.getAsObject();
            answers.push(row);
        }
    }
    stmt.free();

    return answers;
}

//get a specific item - unused
function getItem(item)
{
    stmt = db.prepare("select * from item where itemID = :item");
    var itemRow = stmt.getAsObject({ ':item': item });

    stmt.free();
    return itemRow;
}

//Set user's correct/incorrect response in the db as the user answers - capable of multiple users
function setExperience(user, question, complete)
{
    stmt = db.prepare("select completed from question_user where user_id = $user and question_id = $question");
    completeCheck = stmt.getAsObject({ '$user': user, '$question': question });
    stmt.free();

    //if 0 - updates the entry. if 1 skips this, user doesn't need to be updated. if !exist create entry
    if (completeCheck.completed == 0)
    {
        db.run("update question_user set completed = ? where user_id = ? and question_id = ?",
        [complete, user, question]);
    }
    else if(completeCheck.completed != 1 )
    {
        db.run("INSERT INTO question_user VALUES (?, ?, ?)",
        [user, question, complete]);
    }
}

//get count of questions
function getQuestionCount(lesson)
{
    var stmt = db.prepare("select count(qID) as questionCount from question where lesson_id = $lesson");
    var count = stmt.getAsObject({ '$lesson': lesson });
    stmt.free();
    return count;
}

//get a count of how many questions are correct within a lesson
function getQuestionCorrectCount(start, count)
{
    var stmt = db.prepare("select count(completed) as counter from question_user where completed = 1 and question_id between $start and $count");
    var correctCount = stmt.getAsObject({ '$start': start.qID, '$count': start.qID + count.questionCount - 1 });
    stmt.free();
    return correctCount;
}

//unlock lesson after previous lesson is completed
function unlockLesson(lesson)
{
    db.run("update lesson set unlocked = 1 where lessonID = ? and unlocked = 0",
    [lesson + 1]);

    //begins checking if an 'unlocking cascade' has to occur, if all lessons of a course have been completed
    var stmt = db.prepare("select course_id from lesson where lessonID = $lesson");
    var course = stmt.getAsObject({ '$lesson': lesson });
    stmt.free();

    var stmt = db.prepare("select count(lessonID) as counter from lesson where course_id = $course");
    var lessonCount = stmt.getAsObject({ '$course': course.course_id });
    stmt.free();

    var stmt = db.prepare("select count(unlocked) as counter from lesson where course_id = $course and unlocked = 1");
    var unlockCount = stmt.getAsObject({ '$course': course.course_id });
    stmt.free();

    //unlock the course
    if (unlockCount.counter == lessonCount.counter)
    {
        unlockCourse(course);
    }
}

//unlock course after previous course is completed
function unlockCourse(course)
{
    db.run("update course set unlocked = 1 where courseID = ? and unlocked = 0",
    [course.course_id + 1]);

    //begins checking if an 'unlocking cascade' has to occur, if all courses of a grade have been completed
    var stmt = db.prepare("select grade_id from course where courseID = $course");
    var grade = stmt.getAsObject({ '$course': course.course_id });
    stmt.free();

    var stmt = db.prepare("select count(courseID) as counter from course where grade_id = $grade");
    var courseCount = stmt.getAsObject({ '$grade': grade.grade_id });
    stmt.free();

    var stmt = db.prepare("select count(unlocked) as counter from course where grade_id = $grade and unlocked = 1");
    var unlockCount = stmt.getAsObject({ '$grade': grade.grade_id });
    stmt.free();

    //unlock the grade
    if (unlockCount.counter == courseCount.counter)
    {
        var stmt = db.prepare("select count(gradeID) as counter from grade");
        var gradeCount = stmt.getAsObject({ '$grade': grade.grade_id });
        stmt.free();

        if (grade.grade_id + 1 <= gradeCount.counter - 1)
        {
            db.run("update grade set unlocked = 1 where gradeID = ? and unlocked = 0",
            [grade.grade_id + 1]);
        }
    }
}

//get results from a lesson using question_user table - needs update for multiple users
function getExperience(lesson)
{
    var count = getQuestionCount(lesson);

    var stmt = db.prepare("select qID from question where lesson_id = $lesson limit 1");
    var start = stmt.getAsObject({ '$lesson': lesson });
    stmt.free();

    var stmt = db.prepare("select completed from question_user where question_id between $start and $count");
    stmt.bind({ $start: start.qID, $count: start.qID + count.questionCount - 1 });
    var results = [];

    while (stmt.step())
    {
        var row = stmt.getAsObject();
        results.push(row.completed);
    }
    stmt.free();

    //unlock lessons, courses, grades after a lesson has been completed - cascades
    correctCount = getQuestionCorrectCount(start, count);
    if (correctCount.counter == count.questionCount)
    {
        unlockLesson(lesson);
    }

    return results;
}

//reset a lessons completed questions - not implemented as users can enter previous lessons when ever they want
function resetLesson(lesson)
{
    db.run("update question_user set completed = 0 where lesson_id = ?",
    [lesson]);
}

//load database from memory
function loadDB(str)
{
    var l = str.length,
            arr = new Uint8Array(l);
    for (var i = 0; i < l; i++) arr[i] = str.charCodeAt(i);
    return arr;
}

//save database to memory
function saveDB(arr)
{
    var uarr = new Uint8Array(arr);
    var strings = [], chunksize = 0xffff;
    // There is a maximum stack size. We cannot call String.fromCharCode with as many arguments as we want
    for (var i = 0; i * chunksize < uarr.length; i++) {
        strings.push(String.fromCharCode.apply(null, uarr.subarray(i * chunksize, (i + 1) * chunksize)));
    }
    return strings.join('');
}

/*
//set the local storage on the device - needs dynamic declarations for multiple users - used in app.js
window.localStorage.setItem("mTheory", saveDB(db.export()));

//free the db from memory - done at app unload
db.close();
*/

/*
//Useful for testing, gets a lot of rows at once and prints them - adjust the query as needed
var stmt = db.prepare("SELECT * FROM item");

//basically a foreach
while(stmt.step())
{
    var row = stmt.getAsObject();

    //Print row
    console.log(JSON.stringify(row));

    //Print single column from a single row
    console.log(row.itemID);
}
stmt.free();
*/


/*
//Useful for testing, gets a single row and prints it - adjust the query as needed
var stmt = db.prepare("SELECT * FROM item WHERE itemID = $itemIDval");

//binds values in query
var result = stmt.getAsObject({ '$itemIDval': 1 });
stmt.free();

//print row
console.log(JSON.stringify(result));

//print single value from specified columnn
console.log(result.itemID);
*/